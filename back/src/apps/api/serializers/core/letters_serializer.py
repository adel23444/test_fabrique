from rest_framework.serializers import ModelSerializer

from src.apps.core.models import LettersMailing


class LettersSerializer(ModelSerializer):
    class Meta:
        model = LettersMailing
        fields = '__all__'