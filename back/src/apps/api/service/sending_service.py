from src.apps.celery.tasks import send_data

class SendData:

    def __init__(self, data):
        self.data = data

    def send(self):

        send_data.s(data=self.data)\
            .apply_async(countdown=3)