from django.urls import path
from rest_framework.routers import SimpleRouter

from src.apps.api.v1.core import ClientViewSet, LettersViewSet


app_name = 'api'
router = SimpleRouter()
router.register('client', ClientViewSet, basename='client')
router.register('letters_mailing', ClientViewSet, basename='letters_mailing')


urlpatterns = router.urls