from rest_framework import viewsets

from src.apps.api.serializers.core import ClientSerializer
from src.apps.core.models import Client


class ClientViewSet(viewsets.ModelViewSet):

    serializer_class = ClientSerializer
    queryset = Client.objects.all()