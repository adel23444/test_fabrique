from rest_framework import viewsets

from src.apps.api.serializers.core import LettersSerializer
from src.apps.core.models import LettersMailing


class LettersViewSet(viewsets.ModelViewSet):

    serializer_class = LettersSerializer
    queryset = LettersMailing.objects.all()
