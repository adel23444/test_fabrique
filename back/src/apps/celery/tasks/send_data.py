import requests

from celery import shared_task

from django.conf import settings
from django.core.mail import send_mail


@shared_task(bind=True)
def send_data(self, data):
    print("Sending data to")
    token = settings.CLIENT_TOKEN
    url = settings.CLIENT_LINK
    headers = {
        'Authorization': token
    }
    try:
        response = requests.post(url, headers=headers, json=data)
    except requests.RequestException as e:
        print(f"Recipient error: {e}")
    except requests.ConnectionError() as e:
        raise self.retry(exc=e, countdown=15)
    return response
