from datetime import datetime

from celery import shared_task
from celery.utils.log import get_task_logger

from django.db.models import Q

from src.apps.core.models import LettersMailing, Client, Message
from src.apps.api.service import SendData
from src.apps.core.enums import MessageStatus


@shared_task(name='letters_mailing_send_messages')
def letters_mailing_send_messages():

    now = datetime.now()

    letters = LettersMailing.objects.filter(
        date_letter__day=now.day,
        date_letter__month=now.month,
        date_letter__year=now.year,
        date_letter__hour__gte=now.hour,
        date_letter__minute__gte=now.minute,
        date_letter_stop__hour__lt=now.hour,
        date_letter_stop__minute__lt=now.minute
    )

    for letter in letters:
        clients = Client.objects.filter(Q(phone_code__icontains=letter.filter_field) |
                                        Q(tag__icontains=letter.filter_field))
        for client in clients:
            message = Message(
                message_date=datetime.now(),
                status=MessageStatus.not_send,
                client=client,
                letters_mailing=letter
            )
            send_service = SendData(data={
                'id': message.id,
                'phone': client.phone,
                'text': letter.message_text
            })
            send_service.send()
            message.status = MessageStatus.sended
            message.save()