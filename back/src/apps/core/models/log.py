from django.contrib.admin.models import LogEntry
from django.utils.translation import gettext_lazy as _

ADDITION = 1
CHANGE = 2
DELETION = 3
SEND_MAIL = 4

ACTION_FLAG_CHOICES = (
    (ADDITION, _("Addition")),
    (CHANGE, "Изменение данных"),
    (DELETION, "Удаление данных"),
    (SEND_MAIL, "Отсылка письма"),
)


class ProxyLogEntry(LogEntry):

    class Meta:
        proxy = True
        verbose_name = "Журнал событий"
        verbose_name_plural = "Журнал событий"
