from django.db import models


class LettersMailing(models.Model):

    date_letter = models.DateTimeField(
        verbose_name="Дата и время рассылки",
        help_text="Дата и время рассылки",
        auto_created=True
    )
    message_text = models.TextField(
        verbose_name="Текст сообщения",
        help_text="Текст сообщения",
        null=True,
        blank=True
    )
    filter_field = models.CharField(
        max_length=255,
        verbose_name="Фильтр свойств клиентов",
        help_text="Фильтр свойств клиентов",
        null=True,
        blank=True
    )
    date_letter_stop = models.DateTimeField(
        verbose_name="Дата и время остановки рассылки",
        help_text="Дата и время остановки рассылки",
        null=True,
        blank=True
    )

    def __str__(self):
        return f"Рассылка от {self.date_letter}" if self.date_letter else "Рассылка"

    class Meta:
        verbose_name = "Рассылка"
        verbose_name_plural = "Рассылки сообщений"