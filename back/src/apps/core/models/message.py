from django.db import models

from src.apps.core.enums import MessageStatus


class Message(models.Model):

    message_date = models.DateTimeField(
        verbose_name="Дата и время создания",
        help_text="Дата и время создания",
        auto_created=True
    )
    status = models.CharField(
        verbose_name="Статус отправки",
        help_text="Статус отправки",
        max_length=255,
        null=True,
        blank=True,
        default=MessageStatus.not_send,
        choices=[{x.name, x.value} for x in MessageStatus]
    )
    client = models.ForeignKey(
        to="core.Client",
        related_name="message",
        related_query_name="message",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        verbose_name="Клиент",
        help_text="Клиент"
    )
    letters_mailing = models.ForeignKey(
        to="core.LettersMailing",
        related_name="message",
        related_query_name="message",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        verbose_name="Рассылка",
        help_text="Рассылка"
    )

    class Meta:
        verbose_name = "Сообщение"
        verbose_name_plural = "Сообщения"