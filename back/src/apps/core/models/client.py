from django.db import models
from django.core.validators import RegexValidator


class Client(models.Model):

    phone_regex = RegexValidator(
        regex=r'\d{11,14}',
        message='Номер телефона стоит вводить в формате: "12223333333".'
    )

    phone = models.CharField(
        verbose_name='Номер телефона',
        help_text='Номер телефона',
        max_length=15,
        validators=[phone_regex],
        null=True,
        blank=True,
    )
    phone_code = models.CharField(
        verbose_name='Код оператора',
        help_text='Код оператора',
        max_length=4,
        null=True,
        blank=True
    )
    tag = models.CharField(
        verbose_name="Тэг",
        help_text="Тэг",
        max_length=20,
        null=True,
        blank=True
    )
    timezone = models.CharField(
        verbose_name="Часовой пояс",
        help_text="Часовой пояс",
        max_length=10,
        null=True,
        blank=True
    )

    def __str__(self):
        return f"Клиент {self.phone}"

    class Meta:
        verbose_name = "Клиент"
        verbose_name_plural = "Клиенты"