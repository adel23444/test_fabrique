from django.contrib import admin
from django.urls import reverse
from django.utils.html import format_html

from src.apps.core.models import ProxyLogEntry
from src.apps.core.models.log import ACTION_FLAG_CHOICES
# Register your models here.
from src.apps.core.models import (LettersMailing, Client, Message,
                                  ProxyLogEntry)

admin.site.register(LettersMailing)
admin.site.register(Client)
admin.site.register(Message)


class ActionTypeFilter(admin.SimpleListFilter):
    title = "Тип события"
    parameter_name = "action"

    def lookups(self, request, model_admin):
        return ACTION_FLAG_CHOICES

    def queryset(self, request, queryset):
        value = self.value()
        if value:
            return queryset.filter(action_flag=value)

        return queryset
@admin.register(ProxyLogEntry)
class ProxyLogEntryAdmin(admin.ModelAdmin):
    list_display = ('id', 'action_time', 'user', 'content_type', 'object_repr', 'get_action_name',
                    'message', 'object_button')
    list_filter = ('user', 'content_type', ActionTypeFilter)

    fields = (
        'action_time',
        'user',
        'content_type',
        'object_id',
        'object_repr',
        'get_action_name',
        'message',
        'change_message',
    )

    def message(self, obj):
        return str(obj)

    message.short_description = 'Описание'

    def get_action_name(self, obj):
        if obj.action_flag:
            for (num, value) in ACTION_FLAG_CHOICES:
                if num == obj.action_flag:
                    return value
        return None

    get_action_name.short_description = 'Тип события'
    get_action_name.admin_order_field = 'action_flag'

    def object_button(self, obj):
        o = obj.content_type.get_all_objects_for_this_type(pk=obj.object_id).first()
        if o:
            url = reverse('admin:%s_%s_change' % (o._meta.app_label, o._meta.model_name),
                                                  args=[o.pk])
            href = f"""
            <a href="{url}">Перейти к записи</a>
            """
            return format_html(href)
        return ''

    object_button.allow_tags = True
    object_button.short_description = ''

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def changeform_view(self, request, object_id=None, form_url="", extra_context=None):
        extra_context = extra_context or {}
        obj = ProxyLogEntry.objects.filter(id=object_id).first()
        o = obj.content_type.get_all_objects_for_this_type(pk=obj.object_id).first()
        if o:
            extra_context['button_url'] = reverse('admin:%s_%s_change' % (o._meta.app_label, o._meta.model_name),
                                                  args=[o.pk])
        else:
            extra_context['button_url'] = None
        return super().changeform_view(request, object_id, extra_context=extra_context)
